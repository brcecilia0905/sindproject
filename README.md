
# CadastroAPI Manager
- O teor do projeto consiste em uma Seleção que estou participando, mas ainda terminarei todo os requisitos do projeto.

## Desenvolvimento

* Em fase de desenvolvimento.
* Criação de modelo de dados para o mapeamento de entidades em bancos de dados.
* ainda será implementado as operações de gerenciamento de usuários (Cadastro, leitura, atualização e remoção de pessoas de um sistema).
* Desenvolvimento de testes unitários para validação das funcionalidades.
* Testes via Postman.
* Teste Seleção para SindiOnibus.

## Requisitos
* Nodejs
* @angular/cli
* .Net sdk version 2.2.1 


Para executar o projeto no terminal, vá até a pasta "Sind-App", digite o seguinte comando:

```shell script
npm install
```
Em seguida, vá até a pasta master do projeto, digite o seguinte comando:

```shell script
dotnet build
```

Depois, abra dois terminais, um na pasta "Sind-App" e outro na pasta "Sind-Api", digite o seguinte comando na Sind-Api:

```shell script
dotnet run
```

digite o seguinte comando na Sind-App:

```shell script
ng serve
```

Após executar o comando acima, basta apenas abrir o seguinte endereço e visualizar a execução do projeto:

```
http://localhost:4200

http://localhost:5000/api/values
```
