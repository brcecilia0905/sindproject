using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sind.Domain;
using Sind.Repository;

namespace Sind_Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CadastroController : ControllerBase
    {
        private readonly ISindRepository _repo;

        public CadastroController(ISindRepository repo)
        {
            _repo = repo;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var results = await _repo.GetAllCadastroAsync();
                return Ok(results);
            }
            catch (System.Exception)
            {
                
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }
            
        }

        [HttpPost("upload")]
        public IActionResult upload()
        {
            try
            {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine("Resources", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                if (file.Length > 0)
                {
                    var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName;
                    var fullPath = Path.Combine(pathToSave, filename.Replace("\"", " ").Trim());

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }

                return Ok();
            }
            catch (System.Exception)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }

            // return BadRequest("Erro ao enviar foto");

        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(int Id)
        {
            try
            {
                var results = await _repo.GetAllCadastroAsyncById(Id);
                return Ok(results);
            }
            catch (System.Exception)
            {
                
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }
            
        }

         [HttpPost]
        public async Task<IActionResult> Post(Cadastro model)
        {
            try
            {
                _repo.Add(model);
                if(await _repo.SaveChangeAsync()){
                    return Created($"/api/cadastro/{model.Id}", model);
                }
            }
            catch (System.Exception)
            {
                
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }

            return BadRequest();
            
        }

        [HttpPut]
        public async Task<IActionResult> Put(int Id, Cadastro model)
        {
            try
            {
                var cadastro = await _repo.GetAllCadastroAsyncById(Id);
                if(cadastro == null) return NotFound();
                _repo.Update(model);
                if(await _repo.SaveChangeAsync()){
                    return Created($"/api/cadastro/{model.Id}", model);
                }
            }
            catch (System.Exception)
            {
                
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }

            return BadRequest();
            
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int Id, Cadastro model)
        {
            try
            {
                var cadastro = await _repo.GetAllCadastroAsyncById(Id);
                if(cadastro == null) return NotFound();

                _repo.Delete(cadastro);
                
                if(await _repo.SaveChangeAsync()){
                    return Ok();
                }
            }
            catch (System.Exception)
            {
                
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }

            return BadRequest();
            
        }

    }

    
}