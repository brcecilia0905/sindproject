using AutoMapper;
using Sind.Domain;
using Sind_Api.Dtos;

namespace Sind_Api.helpers
{
    public class AutoMapparprofiles : Profile
    {
        public AutoMapparprofiles()
        {
            CreateMap<Cadastro, CadastroDto>();
        }
    }
}