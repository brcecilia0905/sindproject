export interface Cadastro {
    id: number; 
    nome: string; 
    cpf: string; 
    email: string; 
    dataNasci: Date; 
    imagemURL: string; 
}
