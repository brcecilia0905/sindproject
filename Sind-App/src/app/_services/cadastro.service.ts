import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cadastro } from '../_models/Cadastro';

@Injectable({
  providedIn: 'root'
})
export class CadastroService {
  baseURL = 'http://localhost:5000/api/cadastro';
  

  constructor(private http: HttpClient) { }
  
  getAllCadastro(): Observable<Cadastro[]>{
    return this.http.get<Cadastro[]>(this.baseURL);
  }

  getCadastroByNome(nome: string): Observable<Cadastro[]>{
    return this.http.get<Cadastro[]>(`${this.baseURL}/getByNome/${nome}`);
  }

  getCadastroById(id: number): Observable<Cadastro>{
    return this.http.get<Cadastro>(`${this.baseURL}/${id}`);
  }

  postUpload(file: File[]){
    const fileToUpload = <File>file[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(`${this.baseURL}/upload`, formData);
  }

  postCadastro(cadastro: Cadastro){
    return this.http.post<Cadastro>(this.baseURL, cadastro);
  }
}
