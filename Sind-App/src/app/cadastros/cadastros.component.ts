import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Cadastro } from '../_models/Cadastro';
import { CadastroService } from '../_services/cadastro.service';
import { defineLocale, ptBrLocale } from 'ngx-bootstrap/chronos';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ToastrModule } from 'ngx-toastr';
import { FileDetector } from 'selenium-webdriver';

//import { Cadastro } from '../_models/Cadastro';
defineLocale('pt-br', ptBrLocale);

@Component({
  selector: 'app-cadastros',
  templateUrl: './cadastros.component.html',
  styleUrls: ['./cadastros.component.css']
})
export class CadastrosComponent implements OnInit {

  _filtroLista: string = '';
  get filtroLista(): string {
    return this._filtroLista;
  }
  set filtroLista(value: string){
    this._filtroLista = value;
    this.cadastroFiltrados = this.filtroLista ? this.filtrarCadastros(this.filtroLista) : this.cadastros;
  }

  
  cadastroFiltrados!: Cadastro[];
  cadastros!: Cadastro[];
  cadastro!: Cadastro;
  imagemLargura: number = 50;
  imagemMargem: number = 2;
  mostrarImagem: boolean = false;
  form!: FormGroup;
  file!: File[];
  dataAtual!: string;

  get f(): any {
    return   this.form.controls;
  }

  constructor(private cadastroService: CadastroService,
    private modalService: BsModalService, private fb:FormBuilder,
    private BsLocaleService: BsLocaleService,
    //private toastr: ToastrService
    ) 
       { this.BsLocaleService.use('pt-br'); }
 
  openModal(template: any){
    this.form.reset();
    template.show();
  }

  ngOnInit() {
    this.validation();
    this.getCadastros();
  }

  filtrarCadastros(filtrarPor: string): Cadastro[] {
    filtrarPor = filtrarPor.toLocaleLowerCase();
    return this.cadastros.filter(
      cadastro => cadastro.cpf.toLocaleLowerCase().indexOf(filtrarPor) !== -1
    );
  }

  alternarImagem(){
    this.mostrarImagem = !this.mostrarImagem;
  }

  validation(){
    this.form = this.fb.group({
      nome: ['',Validators.required],
      cpf: ['', Validators.required],
      dataNasci: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      imagemURL: ['', Validators.required],
    });
  }

  onFileChange(event: any){
    const reader = new FileReader();
    if(event.target.files && event.target.files.length){
      this.file = event.target.files;
    }
  }

  salvarAlteracao(template: any){
    if(this.form.valid ){
      this.cadastro = Object.assign({}, this.form.value);
      this.cadastroService.postUpload(this.file).subscribe(
        () => {
          this.dataAtual = new Date().getDate().toString();
          this.getCadastros();
        }
      );

      const nomeArquivo = this.cadastro.imagemURL.split('\\', 3);
      this.cadastro.imagemURL = nomeArquivo[2];

      this.cadastroService.postCadastro(this.cadastro).subscribe(
        (novoCadastro: Cadastro) => {
          console.log(novoCadastro);
          template.hide();
          this.getCadastros();
        }, (error: any) =>{
          console.log(error);
        }
      );
    } else{
      const nomeArquivo = this.cadastro.imagemURL.split('\\', 3);
      this.cadastro.imagemURL = nomeArquivo[2];
    }
  }

  getCadastros(){
    this.cadastroService.getAllCadastro().subscribe(
      (_cadastros: Cadastro[]) => {
       this.cadastros = _cadastros;
       this.cadastroFiltrados = this.cadastros;
       console.log(_cadastros);
      }, error => { 
        console.log(error);
      });
  }

}
