using System;


namespace Sind.Domain
{
    public class Cadastro
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string DataNasci { get; set; }
        public string ImagemURL { get; set; }
        
    }
}