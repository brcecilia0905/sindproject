using System.Threading.Tasks;
using Sind.Domain;

namespace Sind.Repository
{
    public interface ISindRepository
    {
        //GERAL
         void Add<T>(T entity) where T : class;

         void Update<T>(T entity) where T : class;
         void Delete<T>(T entity) where T : class;
         Task<bool> SaveChangeAsync();
         //CADASTROS

         Task<Cadastro[]> GetAllCadastroAsyncByNome(string nome);
         Task<Cadastro[]> GetAllCadastroAsync();
         Task<Cadastro> GetAllCadastroAsyncById(int id);
    }
}