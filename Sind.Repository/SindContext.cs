using Microsoft.EntityFrameworkCore;
using Sind.Domain;

namespace Sind.Repository
{
    public class SindContext : DbContext
    {
        public SindContext(DbContextOptions<SindContext> options) : base (options) {}
        
         public DbSet<Cadastro> Cadastros { get; set; }   
        
    }
}