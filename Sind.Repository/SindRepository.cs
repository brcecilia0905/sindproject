using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sind.Domain;

namespace Sind.Repository
{
    public class SindRepository : ISindRepository
    {
        private readonly SindContext _context;
        public SindRepository(SindContext context)
        {
            _context = context;
            _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

        }

        //GERAIS
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }
        public void Update<T>(T entity) where T : class
        {
            _context.Update(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }
        public async Task<bool> SaveChangeAsync()
        {
            return (await _context.SaveChangesAsync()) >0;
        }

        //CADASTROS

        public async Task<Cadastro[]> GetAllCadastroAsync()
        {
            IQueryable<Cadastro> query = _context.Cadastros;
            return await query.ToArrayAsync();
                
        }

        public async Task<Cadastro> GetAllCadastroAsyncById(int id)
        {
            IQueryable<Cadastro> query = _context.Cadastros;
            query= query.Where(c => c.Id == id);

            return await query.FirstOrDefaultAsync();
        }

        public async Task<Cadastro[]> GetAllCadastroAsyncByNome(string nome)
        {
            IQueryable<Cadastro> query = _context.Cadastros;
            query= query.Where(c => c.Nome.Contains(nome));

            return await query.ToArrayAsync();
        }

        public Task<Cadastro[]> GetAllCadastroAsync(bool v)
        {
            throw new System.NotImplementedException();
        }
    }
}